# haiku-python-c++.wrapper
v. 0.1.0-0

Simple C++ wrapper to integrate Python apps into the Haiku OS environment

Remember to create a /deskbar/menu/Applications folder inside the data folder, and place the symlink to the application.


__WARNING:__
This is a work in progress. This first approach has issues when closing the
application window, namely:
* The application window is closed, and the application disappears from the
  running process list, but the Python runtime process is still there


